<!DOCTYPE html>
<html>

<head>
    <title>Bob's Auto Parts - Order Results</title>
</head>

<body>

    <h1>Bob's Auto Parts</h1>
    <h2>Order Results</h2>

    <div>
        <!-- this paragraph is for the PHP script-->


        <?php
        //create shorter variable names for the quantity of the materials
        $tireqty=$_POST['tireqty'];
        $oilqty=$_POST['oilqty'];
        $sparkqty=$_POST['sparkqty'];


        echo "order is processed at " . date('H:i, jS F Y.');

        //to make the script do something visible
    
        echo '<p>Your order is as follows:</p>';
        echo htmlspecialchars($tireqty).' tires <br>';
        echo htmlspecialchars($oilqty). ' oils <br>';
        echo htmlspecialchars($sparkqty).' sparkplugs';
        
    


        ?>

    </div>

</body>

</html>